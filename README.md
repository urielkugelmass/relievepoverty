# RelievePoverty

## Name, EID, GitlabID
Albert Luu - ATL822 - @aluu  
Colin Frick - CRF2345 - @cfrick16  
Daniel Chruscielski - DJC3499 - @dchruscielski  
Evan Weiss - EAW2732 - @eweiss97  
Uriel Kugelmass - UK626 - @urielkugelmass  
Yoav Ilan - YI637 - @YoavIlan  

## Git SHA
Phase 1: 5e93483a681e01f3cce91f96e257ead1e3d01a30
Phase 2: 9d323736a70d4d9109d6a5e3ad6f76192c7c0a72
Phase 3: d33a11b6a06ad0fb2bfdf28f5393d156b813304a
Phase 4: 824a0fd7babca6a8e196116b38f1e6f82c920275

## Website
[https://relievepoverty.me/](https://relievepoverty.me/)

## Pipelines
[https://gitlab.com/urielkugelmass/relievepoverty/pipelines](https://gitlab.com/urielkugelmass/relievepoverty/pipelines)

## Estimated Completion Time for Phase 1:
Albert - 20 hours  
Colin - 20 hours  
Daniel - 30 hours  
Evan - 25 hours  
Uriel - 30 hours  
Yoav - 15 hours  

## Actual Completion Time for Phase 1:
Albert - 16 hours  
Colin - 17 hours  
Daniel - 19 hours  
Evan - 18 hours  
Uriel - 16 hours  
Yoav - 18 hours  

## Estimated Completion Time for Phase 2:
Albert - 35 hours  
Colin - 40 hours  
Daniel - 40 hours  
Evan - 35 hours  
Uriel - 30 hours  
Yoav - 40 hours  

## Actual Completion Time for Phase 2:
Albert - 48 hours  
Colin - 55 hours  
Daniel - 54 hours  
Evan - 56 hours  
Uriel - 57 hours  
Yoav - 52 hours  

## Estimated Completion Time for Phase 3:
Albert - 10 hours  
Colin - 15 hours  
Daniel - 20 hours  
Evan - 15 hours  
Uriel - 10 hours  
Yoav - 10 hours  

## Actual Completion Time for Phase 3:
Albert - 12 hours  
Colin - 20 hours  
Daniel - 16 hours  
Evan - 11 hours  
Uriel - 15 hours  
Yoav - 13 hours 

## Estimated Completion Time for Phase 4:
Albert - 14 hours  
Colin - 8 hours  
Daniel - 20 hours  
Evan - 3 hours  
Uriel - 6 hours  
Yoav - 10 hours  

## Actual Completion Time for Phase 4:
Albert - 7 hours  
Colin - 7 hours  
Daniel - 7 hours  
Evan - 6 hours  
Uriel - 6 hours  
Yoav - 7 hours 

## Comments
